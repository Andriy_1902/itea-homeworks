/*

    Задание 3:

    1. Создать ф-ю констурктор которая создаст новую собаку у которой есть имя и порода
    2. Обьект должен иметь пару свойств (Имя, порода, status)
    3. Функцию которая производит манипуляцию со свойствами (Собака бежит), (Собака есть)
    4. Функция которая перебором выводит все свойства

    // Перебор свойств и методов обьекта
    for (key in obj) {
      console.log( key, obj[key] );
      /* ... делать что-то с obj[key] ...
    // }
*/

var bind_block = document.querySelector(".bind_block");
bind_block.style.display = "none";

function createDog(name,breed,status) {
	this.name = name;
	this.breed = breed;
	this.status = status;
    this.manipulation = function (status) {
      console.log(this.name + " - " +  this.status + "\n");
      console.log("-----------------------------------");
  }

}


let funkDog1 = new createDog("Lessi","chao-chao", "sit");
let funkDog2 = new createDog("Lwdwdwdwdsi","chdwdo", "run");
funkDog1.manipulation();
for (key in funkDog1) {
      console.log( key,"-", funkDog1[key] );

      console.log( "свойство: " + key  + "\n"+ "значение: " + funkDog1[key] );
      console.log("-----------------------------------");
  }



