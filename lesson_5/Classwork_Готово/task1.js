/*

    Задание 1:

    Написать обьект Train у которого будут свойства:
    -имя,
    -скорость езды
    -количество пассажиров
    Методы:
    Ехать -> Поезд {name} везет { количество пассажиров} со скоростью {speed}
    Стоять -> Поезд {name} остановился. Скорость {speed}
    Подобрать пассажиров -> Увеличивает кол-во пассажиров на Х
*/

var bind_block = document.querySelector(".bind_block");
bind_block.style.display = "none";

var Train = {
	name: "Intersity",
	speed: "50 m/s",
	numberPeople: 20,
	move: function(){
		console.log("Поезд " + this.name + " везет " + this.numberPeople + " со скоростью " + this.speed);
	},
	stop: function(){
		this.speed = "0 m/s";
		console.log("Поезд " + this.name + " остановился. Скорость " + this.speed);
	},
	getPasinger: function(x){
		this.numberPeople += x;
		console.log("Подобрал " + this.numberPeople + " пассажиров ");
	}
}
Train.move();
Train.stop();
Train.getPasinger(300);
