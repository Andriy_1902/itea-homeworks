/*

  Задание:

    1. Написать конструктор объекта комментария который принимает 3 аргумента
    ( имя, текст сообщения, ссылка на аватаку);

    {
      name: '',
      text: '',
      avatarUrl: '...jpg'
      likes: 0
    }
      + Создать прототип, в котором будет содержаться ссылка на картинку по умлочанию
      + В прототипе должен быть метод который увеличивает счетик лайков

    var myComment1 = new Comment(...);

    2. Создать массив из 4х комментариев.
    var CommentsArray = [myComment1, myComment2...]

    3. Созадть функцию конструктор, которая принимает массив коментариев.
      И выводит каждый из них на страничку.

    <div id="CommentsFeed"></div>

*/

function Comment(name,text,url,likes){
	this.name = name;
	this.text = text;
	this.url = url;
	this.likes = likes;
}
var myComment1 = new Comment("Max"," понравилась картинка №1...","https://lifeimg.pravda.com/images1.jpg",0);
var myComment2 = new Comment("Jimy"," понравилась картинка №2...","https://lifeimg.pravda.com/images2.jpg",0);
var myComment3 = new Comment("Katy"," понравилась картинка №3...","https://lifeimg.pravda.com/images3.jpg",0);
var myComment4 = new Comment("Lily"," понравилась картинка №4...","https://lifeimg.pravda.com/images4.jpg",0);
var CommentsArray = [myComment1, myComment2,myComment3,myComment4];


console.log(CommentsArray[0].name);


var CommentsFeed = document.getElementById("CommentsFeed");

function showComment(){
	this.showFunk = function(){
		console.log("TRUE");
		
		for(let i=0; i<CommentsArray.length;i++){
			let elem = document.createElement("div");
			elem.innerHTML =CommentsArray[i].name + CommentsArray[i].text + " " + CommentsArray[i].url  + "----likes: " + CommentsArray[i].likes;
			CommentsFeed.appendChild(elem);			
		}
		
	}
	
}

function proto(){
	
	this.url = "https://lifeimg.pravda.com/default.jpg";
	this.increaselikes = function(){
		this.likes++;
	} 
}


var result = new showComment();
result.showFunk();


var protoName = new proto();
Object.setPrototypeOf( myComment1, protoName);
Object.setPrototypeOf( myComment2, protoName);
Object.setPrototypeOf( myComment3, protoName);
Object.setPrototypeOf( myComment4, protoName);

myComment1.increaselikes();
myComment2.increaselikes();
myComment3.increaselikes();
myComment4.increaselikes();
result.showFunk();

myComment1.increaselikes();
myComment2.increaselikes();
myComment3.increaselikes();
myComment4.increaselikes();
result.showFunk();